#!/bin/bash
# ACE - December 13, 2013

set -e

# Run the multiple complexity analyses on the devrand events.
function run-devrand-complexity()
{
  PLATFORM=$1
  COUNT=$2
  RESULTS=results
  LOGS=$PLATFORM-logs
  TABLE=$RESULTS/$PLATFORM-event-table

  # Create a merge-file if there isn't one already.
  if [ ! -f $TABLE ]; then
    echo "Creating event table $TABLE"
    ./process-devurand-inputs.py $TABLE $COUNT $LOGS/*.json
  fi

  # Create a CSV that shows aggregate complexity for each sequence.
  CSV=$RESULTS/$PLATFORM-ks-complexity.csv
  if [ ! -f $CSV ]; then
    echo "Calculating complexity using ks-test: $CSV"
    ./ks-complexity.py $TABLE > $CSV 
  fi

  # Run with chi-square test.
  CSV=$RESULTS/$PLATFORM-chi2-complexity.csv
  if [ ! -f $CSV ]; then
    echo "Calculating complexity using chi2-test: $CSV"
    ./cs-complexity.py $TABLE > $CSV 
  fi

  # Run with correlation coefficient tests
  CSV=$RESULTS/$PLATFORM-pearson-complexity.csv
  if [ ! -f $CSV ]; then
    echo "Calculating complexity using correlation coefficient tests: $CSV"
    ./cc-complexity.py $TABLE > $CSV 
  fi
}

# Run the min-entropy anlaysis script on data from a given platform
function run-min-entropy()
{
  PLATFORM=$1
  COUNT=$2
  RESULTS=results
  LOGS=$PLATFORM-logs
  ETABLE=$RESULTS/$PLATFORM-event-table-$COUNT
  ATABLE=$RESULTS/$PLATFORM-alpha-table-$COUNT

  echo $ETABLE
  
  # Create an events table
  if [ ! -f $ETABLE ]; then
    echo "Creating event table $ETABLE"
    ./process-devurand-inputs.py $ETABLE $COUNT $LOGS/*.json
  fi

  # Create the alpha table using the KS test.
  if [ ! -f $ATABLE ]; then
    echo "Creating alpha table $ATABLE"
    ./ks-complexity.py --alphas=$ATABLE $ETABLE 
  fi

  # Run the min entropy analysis
  CSV=$RESULTS/$PLATFORM-min-entropy-$COUNT.csv
  if [ ! -f $CSV ]; then
    echo "Running min-entropy analysis: $CSV"
    ./min-entropy-analysis.py $ATABLE $ETABLE > $CSV
  fi
}

# Run the ks-complexity for GRI outputs.
function run-gri-complexity()
{
  PLATFORM=$1
  COUNT=$2
  RESULTS=results
  LOGS=$PLATFORM-logs
  TABLE=$RESULTS/$PLATFORM-gri-event-table

  # Create the event table, if needed.
  if [ ! -f $TABLE ]; then
    echo "Creating GRI event table $TABLE"
    ./process-gri-inputs.py $TABLE $COUNT $LOGS/*.json
  fi

  # Create a CSV that shows aggregate complexity for each sequence.
  CSV=$RESULTS/$PLATFORM-gri-ks-complexity.csv
  if [ ! -f $CSV ]; then
    echo "Calculating GRI complexity using ks-test: $CSV"
    ./ks-complexity.py --gri $TABLE > $CSV 
  fi
}

# Count sequence length for a given platform
function sequence-length()
{
  PLATFORM=$1
  ARCH=$2
  MS=$3
  RESULTS=results
  LOGS=$PLATFORM-logs
  CSV=$RESULTS/$PLATFORM-sequence-length-$MS.csv

  # Create a CSV that shows aggregate complexity for each sequence.
  if [ ! -f $CSV ]; then
    echo "Calculating sequence length: $CSV"
    ./count-sequence-length.py $MS $ARCH $LOGS/*.json > $CSV 
  fi
}

# Count sequence length
MS=5000
sequence-length xen xen $MS
sequence-length vmware xen $MS
sequence-length native xen $MS
sequence-length ec2 ec2 $MS

# I know these last two look wrong, but these VMware and native 
# data were collected on the 'xen' architecture (host sc2)

##
## dev-rand complexity using min-alpha ks test
##
OUTPUTS=15
run-devrand-complexity xen $OUTPUTS
run-devrand-complexity ec2 $OUTPUTS
run-devrand-complexity vmware $OUTPUTS
run-devrand-complexity ww-xen $OUTPUTS


# Min-entropy analysis
OUTPUTS=200
run-min-entropy xen $OUTPUTS
run-min-entropy ec2 $OUTPUTS
run-min-entropy vmware $OUTPUTS
run-min-entropy ww-xen $OUTPUTS
run-min-entropy native $OUTPUTS

# Run native for 200 outputs
OUTPUTS=200
run-devrand-complexity native $OUTPUTS


# Run 10 outputs for xen reset
OUTPUTS=10
run-devrand-complexity xen-reset $OUTPUTS

OUTPUTS=15
run-devrand-complexity ww-xen-reset $OUTPUTS

##
## GRI complexity using KS test
##
OUTPUTS=50
run-gri-complexity xen $OUTPUTS
run-gri-complexity ec2 $OUTPUTS
run-gri-complexity vmware $OUTPUTS
run-gri-complexity native $OUTPUTS
run-gri-complexity xen-reset $OUTPUTS
