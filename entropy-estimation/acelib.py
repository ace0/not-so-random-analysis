#!/usr/bin/python
# acelib.py
#
# ACE - April 27, 2013

"""
Contains general-purpose library routines.
"""

import collections, fileinput, json, logging, numpy, os, re, sys
import cPickle

def doubleShuffle(a,b):
  "Shuffles two lists in unison.  Each list is shuffled in-place."
  # c for combined.
  c = zip(a,b)
  random.shuffle(c)
  a[:], b[:] = zip(*c)


def basicStats(values):
  """
  Produces basic stats from a list of numeric @values:
  (min, max, median, mean, stddev)
  """
  array = numpy.array(values)
  return (min(values), max(values), numpy.median(array), 
    numpy.mean(array), numpy.std(array))


def transpose(matrix):
  """
  Transpose a (possibly jagged) matrix to produce another, (possibly jagged) matrix.
  """
  return map(lambda *row: list(row), *matrix)


def pickle(filename, data):
  """ Writes data to a pickle file. """
  with open(filename, 'wb') as output: 
    cPickle.dump(data, output)


def unpickle(filename):
  """ Reads a single object from a pickle file. """
  with open(filename, 'rb') as f:
    return cPickle.load(f)


def commas(values): 
  "Create a comma-delimited string from any list"
  return ",".join(map(str, values))


def csvPrint(fields, d=None):
  """
  Prints a CSV record from a dictionary to STDOUT.
  Header row: If @d is ommitted, prints a header row using @fields.
  Data row:   If dictionary @d is provided, prints the values from 
  @d associated with the keys from @fields (in the order of @fields).
  """
  # Header row
  if not d: 
    print commas(fields)                

  # Record row  
  else:
    table = collections.defaultdict(int, d)
    print commas([table[key] for key in fields])


def ml(l): 
  "Convert an array of objects into a multiline string"
  return "\n".join(l) 


def pprint(d):
  """
  Pretty-print a dictionary or list with each entry on a seperate line and the
  keys in sorted order.
  """
  if d is dict: pprintd(d)
  else: pprintl(d)


def pprintd(d):
  """
  Pretty-print a dictionary with each entry on a seperate line and the
  keys in sorted order.
  """
  for k in sorted(d.keys()):
    print k, ":", d[k]


def pprintl(values):
  "Pretty-print a list with each entry on a seperate line."
  for v in values:
    print v


def warn(message):
  "Prints a warnning @message to stderr"
  sys.stderr.write("Warning: {0}\n".format(message))


def critical(message):
  "Prints an error message to STDERR and exits with an error code of 1"
  sys.stderr.write("Error: {0}\n".format(message))
  sys.exit(1)


def split(text, length):
  "Split a string into chunks of the specified length"
  return [text[i:i+length] for i in range(0, len(text), length)]


def parseCommandLine():
  """
  Seperates options and positional command line arguments.
  returns: (optionsDict, positionalList)
  optionsDict:  A default dictionary of optional arguments read from the command line.
  positionalList: A list of the positional arguments from the command line.
  """
  optionsDict = collections.defaultdict(lambda: False)
  positionalList = []

  # Quick closure that splits a key-value arguments if it contains an equal sign.
  # Otherwise, it returns just (key, True)
  # In both cases, leading dashes are removed.
  def keyValue(argument):
      # If it has an equal sign, split it into key and value.
      if '=' in argument:
        key, value = argument.split('=')
      else:
        key = argument
        value = True
      # Strip any leading dashes.
      return (key.strip('-'), value)
  ## keyValue

  # Examine each argument
  for argument in sys.argv[1:]:

    # Check for a leading dash.
    if argument.startswith('-'):
      # Split the argument, if necessary, and store it.
      key, value = keyValue(argument)
      optionsDict[key] = value
      
    # Otherwise, put it into the positional argument list
    else:
      positionalList.append(argument)

  return (optionsDict, positionalList)


def printUsage(signature, description = None, optionsDict = None):
  """
  Prints a usage statement and exits the application:
  Usage: scriptName [OPTIONS] <signature>
  <description>
  <args[0]>
  <args[1]>
  ...
  signature:    The expected command line arguments
  description:  Description of the script's functionality
  optionsDict:  Dictionary of options and descriptions.
  """
  # Print the usage
  script = os.path.basename(sys.argv[0])
  print >> sys.stderr, \
    "Usage: {0}{1}{2}".format(script, ' [OPTIONS] ' if optionsDict else ' ', signature)
  
  # Print the description.
  if description: print description
  
  # Print any options.
  if optionsDict:
    print >> sys.stderr
    print >> sys.stderr, "OPTIONS"
    print >> sys.stderr, \
      ml( [ "{0:20} {1}".format(key, value) for (key, value) in optionsDict.items() ] )

  sys.exit(1)


def processFiles(files, 
                 processLine,
                 outputExtension = None,
                 onNewFile = None, 
                 printReadingFile = True,
                 addNewLine = True):
  """
  Process a list of files line-by-line.  If an output extension is specified, 
  non-empty output is written to an output file with the same basename, but with 
  an additional extension.

  files:            A list of inputs filenames to read.
  processLine:      A function to process each line of the files.
  outputExension:   If provided, send output to a file with this additional extension.
  onNewFile:        A function that is called whenever a new file is opened.
  printReadingFile: Turns on/off progress reported to STDOUT.
  addNewLine:       Automatically add a newline with each non-empty call to processLine
                    when output is being sent to a new output file.
  """
  # Write output to this file.
  outputfile = None

  # Set an end-of-line character as requested.
  eol = '\n' if addNewLine else ''

  # Read each input file
  for inputfile in files:
    # Print progress to STDOUT
    if printReadingFile: print "Reading", inputfile

    # Tell the caller that we've opened a new file.
    header = None
    if onNewFile: 
      header = onNewFile(inputfile)

    # If the caller requested an output file then close the previous one
    # and open a new one.
    if outputExtension: 
      if outputfile: outputfile.close()
      outputfile = open(inputfile + '.' + outputExtension, 'w')

      # If the caller provided a header when the file was opened, send it to the output file.
      if header: 
        outputfile.write(header + eol)

    # Open the input file and read it line-by-line
    for line in open(inputfile, 'r'):
      # Process this line.
      text = processLine(line.strip())

      # Send any non-empty output to the file if requested.
      if text and outputfile:
        outputfile.write(text + eol)


def processJsonFiles(files, 
                    processLine,
                    outputExtension = None,
                    onNewFile = None, 
                    printReadingFile = True,
                    addNewLine = True):
  """
  Read a list of JSON files and process each line with the specified process function.

  files:            A list of filenames of JSON files to read.
  processLine:      A function to process each line of the files of the form: 
                    processLine(jsonDict)
  outputExension:   If provided, a new output file is opened for each input file and 
                    all return
                    values from processLine() are written to this file.
  onNewFile:        Called whenever a new file is opened.  Form: onNewFile()
  printReadingFile: Toggle 'Reading file-1.txt' printed to STDOUT.
  addNewLine:       Automatically add a newline to all output sent to output files.
  """
  # Convert each line from JSON before sending to the requested process function
  def jsonFromLine(line): return processLine(json.loads(line))
  processFiles(files, jsonFromLine, outputExtension, onNewFile, 
               printReadingFile, addNewLine)


def run(function=None):
  "Calls a starting function and passes all command-line arguments"
  function(sys.argv[1:])


def parseTraceLogEntry(line):
  """
  Reads a single line (entry) from a trace-log, and puts the 
  field names and values into a dictionary.  If the line doesn't
  match the expected format, returns None.
  """
  table = {}
  match = re.match( r'^.*{(.*)}.*$', line)

  # We can't translate bad input
  if match == None: return None

  # Split the remaining fields into a list
  fields = re.findall( r'[^\s,:=]+', match.group(1))
  
  # Insert them pairwise into the table
  for (k, v) in zip(fields[0::2], fields[1::2]):
    if (v.isdigit()): table[k.strip()] = int(v) # Check for numbers
    else: table[k.strip()] = v.strip()          # Otherwise, use a string
  
  # Done!
  return table

 
def basename(path):
  """
  Retrieves filename with the path and extension removed.
  """
  (_, file) = os.path.split(path)
  (base, _) = os.path.splitext(file)
  return base


def read(filename):
  "Read the contents of a file and close the file"
  with file(filename) as f: return f.read()

