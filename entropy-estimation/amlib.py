#!/usr/bin/python
# entropy-from-sequence.py
#
# ACE - October 7, 2013

import acelib, itertools, math, numpy, operator, scipy.stats

"""
Attack model library: common routines for analyzing attack complexity.
"""

def lg(v): 
  "Log base 2 computation."
  return math.log(v, 2)


def rshiftMatrix(matrix, shift):
  """
  Right shift all (positive) values in a @matrix by the specified
  @shift positions.
  """
  def rshift(x):
    return x if x < 0 else (x >> shift)

  return [ map(rshift, row) for row in matrix ]  


def maskLowerBits(values, bits):
  "Masks all items in a list of @values to the lower @bits."
  mask = getMask(bits)
  return [ (v & mask) for v in values ]


def getMask(lowerbits):
  "Generates a mask for the given number of @lowerbits."
  return (1 << lowerbits) - 1


def maxCorrelation(matrix, depth=None):
  """
  Computes the maximum correlation coefficients for any given set of events over a 
  (transposed)
  @martix of cycle counters (or delta cycles) up a given @depth. (c=cycles)
  matrix = [ Event 1 : [c, c, ... ],
             Event 2 : [c, c, ... ], 
             ...
           ]
           
  Returns a list of the maximum CC for each event with each previous event.
  [ CC1, CC2, ... ]
  Where CCi is the maximum CC with any previous event.
  """
  # If no depth specified, compute the all levels.
  if not depth:
    depth = len(matrix)

  # Start with all zeroes.
  maxCC = [0]*depth

  # Compute CC for all pairs of events.
  for i, j in itertools.combinations(range(depth), 2):
    # Keep the maximum CC and assign it to the later event (event j).
    cc = scipyCC(matrix[i], matrix[j])
    maxCC[j] = max(maxCC[j], cc)

  return maxCC


def correlationTest(a, b, cutoff=0.4):
  """
  Tests whether the Pearson correlation coefficient for lists @a and @b is
  strictly less than @cutoff.
  """
  return (scipyCC(a, b) < 0.4)


def scipyCC(a, b):
  "Computes Pearson correlation coefficient for lists @a, @b using scipy library."
  cc, _ = scipy.stats.pearsonr(numpy.array(a),numpy.array(b))
  return cc


def removeOnesies(values):
  "Removes singleton and empty lists from a matrix."
  return [ v for v in values if v and len(v) > 1 ]


def shave(matrix):
  """ 
  'Shaves' a jagged @matrix into a regular matrix.  Removes any rows that are different in length
  than the first row.
  """
  # Empty matrices are already regular.
  if not matrix: return matrix
  l = len(matrix[0])  
  return [ row for row in matrix if row and len(row) == l ]


def excludeInputs(matrix, ccList, threshold):
  """
  Removes rows from @matrix when the corresponding CC value in @cctable is at or above
  a @threshold value.
  """
  return [ matrix[i] for (i, cc) in enumerate(ccList) if cc < threshold ]
