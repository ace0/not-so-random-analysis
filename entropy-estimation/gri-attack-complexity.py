#!/usr/bin/python
# ACE - October 7, 2013

import acelib, math, random
from attacklib import *

def main(arguments):
  """
  Compute GRI attack complexity and expected success rate on a given set of traces in JSON format.
  """
  if len(arguments) < 4: 
    acelib.printUsage('Max-depth Coverage-fraction <JSON traces>...',  
    'Compute GRI attack complexity for the specified max depth and coverage fraction')

  # Grab our arguments.
  maxDepth, coverage = (int(arguments[0]), float(arguments[1]))

  # Split the set randomly (but deterministically) into testing/training sets
  files = arguments[2:]
  
  # Get the relative cycle counts and transpose the resulting matrix.
  matrix = relativeCyclesFromFiles(files)
  matrix = acelib.transpose(matrix)

  # Emit CSV header.
  print 'Depth, Attack Complexity (log2)'

  # Compute attack ranges and complexity up to the max depth.
  # d for depth.
  for d in range(1, maxDepth+1):
    # c for (attack) complexity
    c  = attackComplexityAtDepth(matrix, coverage, d)
    
    # Emit a CSV record.
    print '%d,%d' % (d,c)
  ## for each depth
## main

def attackComplexityAtDepth(matrix, coverage, depth):
  """
  Computes attack complexity (log2) for given @depth, overall @coverage,
  and transposed @matrix of cycle counters.
  """
  # C for complexity.
  c = complexityFromRanges(train(matrix, coverage, depth))

  # Return result in log2
  return math.log(c, 2)
## attackComplexityAtDepth
# Run our application.
acelib.run(main)
