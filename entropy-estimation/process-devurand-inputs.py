#!/usr/bin/python
# extract-sequence-cycles.py
#
# ACE - October 7, 2013

import acelib, collections, json, os

description = \
"""
Reads a JSON trace file, extracts cycle counters for devurand inputs and output values,
and pickles the resulting table that maps events to a matrix of observed relative 
cycle counters.
"""

# Function names of input and output events.
inputEvents = ['add_interrupt_randomness', 'add_disk_randomness', 
  'add_input_randomness', 'add_device_randomness', 'inject_cycle_entropy']
outputEvents = ['get_random_bytes', 'urandom_read']


def main(unused):
  """
  Read all JSON traces and extract cycle counters for \devurand inputs and outputs.
  """
  # Process command line arguments.
  options, args = acelib.parseCommandLine()
  
  # Check for at least two file arguments an ensure the output file doesn't 
  # already exist.
  if len(args) < 3 or os.path.isfile(args[0]):
    acelib.printUsage('[--matrix-only] <output file> <max outputs> '\
                        '<JSON trace args>...',   
            description)

  # Grab arguments.
  output, maxOutputs, traces = args[0], int(args[1]), args[2:]
    
  # Change our processing behavior for each based on whether we are seperating events
  # into a table or just using a single matrix.
  if 'matrix-only' in options:
    # Our master collection is just a single matrix of inputs cycles only
    seperate=False
    results = []
    append = lambda r, reader: r.append(reader.inputCycles)
    transpose = lambda matrix: acelib.transpose(matrix)

  else:
    # Append results from each file to a master dictionary.
    seperate=True
    results = {}
    append = lambda r, reader: merge(r, reader.eventTable)

    # Transpose each matrix individually when we're done.
    transpose = lambda table: transposeTable(table)
  
  # Process each trace file.
  for f in traces:
    # Read all the input cycle counters and add them to the matrix.
    reader = CycleReader(maxOutputs)
    reader.read(f)
    append(results, reader)

  # Transpose the results and write them to a file.
  acelib.pickle(output, transpose(results))


def transposeTable(d):
  """
  Transposes each matrix in an event table of matrices and returns a new table.
  """
  table = {}
  for key, matrix in d.items():
    table[key] = acelib.transpose(matrix)

    for row in table[key]:
      print key, len(row)

  return table
  

def merge(master, d):
  """
  Merges the contents of event table @d into a @master event table.
  """
  for event, cycleList in d.items():
    # Insert new events into the master table as a matrix.
    if event not in master:
      master[event] = [ cycleList ]

    # Otherwise, append events to the existing matrix.
    else:
      master[event].append(cycleList) 


class CycleReader(object):
  """
  Captures relative cycle counters for input and output events.  Stops after
  processing @maxOutputs.
  """
  # For now, only grab inputs until we see the second output.

  def __init__(self, maxOutputs):
    # A list of relative cycles for each input event.
    self.inputCycles = []
    self.eventTable = collections.defaultdict(list)
    self._start = None
    self._outputCount = 0
    self.maxOutputs = maxOutputs


  def read(self, filename):
    """
    Reads a JSON file and extracts cycle counters from input events.
    """
    with open(filename, 'r') as f:
      # Read each line in the file.
      for line in f:
        # Quit reading once we hit our maximum number of output events.
        if self._outputCount >= self.maxOutputs:
          return

        # Convert from JSON to a record (dictionary) and process this record.
        record = json.loads(line)
        cycles = self.relativeCycles(record)
        if cycles is not None:
          self._processEvent(cycles, record)
      ## for each line
    ## with file


  def relativeCycles(self, record):
    """
    Grabs relative cycle counters of input events from a trace record.
    """
    # Ignore records with an 'events' field.
    # They are duplicative to the other records that we process.
    if 'event' in record: 
      return None

    # Note the first cycle counter.
    if self._start is None: 
      self._start = getCycles(record)

    # Report cycle counters only for input/output events.
    if (record['function'] in inputEvents or
        record['function'] in outputEvents):
      return getCycles(record) - self._start

  def _processEvent(self, cycles, record):
    """
    Stores cycle counters in a dictionary seperated by indexed by event type.
    """
    # Store all input cycle counters in a matrix.
    if record['function'] in inputEvents:
      self.inputCycles.append(cycles)

    # Count each output event.
    if record['function'] in outputEvents:
      self._outputCount += 1
    
    # Get a dictionary key based on the record's event type.
    event = getLabel(record)
    
    # Make sure it's valid and insert it into the dictionary.
    if event:
      self.eventTable[event].append(cycles)
    else:
      acelib.warn('Could not decode record into an input event label: ' + str(record))
## CycleReader


def getLabel(record):
  "Generates a label for each RNG input or output event."
  if record['function'] == 'add_interrupt_randomness': return ('IRQ-%s' % record['irq'])
  if record['function'] == 'add_disk_randomness'     : return 'Disk'
  if record['function'] == 'add_device_randomness'   : return 'Device'
  if record['function'] == 'add_input_randomness'    : return 'Input'
  if record['function'] == 'inject_cycle_entropy'    : return 'Bootstrap'
  if record['function'] in outputEvents              : return 'Output'
  return None
  

def getCycles(record):
  """
  Grabs the absolute cycle counter from a trace giving preference to 'cycles' over 'cycle_count'
  when it's available.
  """
  # Cycle counts.
  if 'cycles' in record:      
    return record['cycles']

  if 'cycle_count' in record: 
    return record['cycle_count']
  
  # Oh well - nothing here to report.
  return None


# Run our application
acelib.run(main)
