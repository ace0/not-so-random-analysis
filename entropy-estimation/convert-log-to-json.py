#!/usr/bin/python
# log-to-json.py
# Converts an entropy log file to proper JSON format.
#
# ACE - February 14, 2013

import acelib, json, re, sys

def main(arguments):
  """Run our application"""
  # Check for 1 or more arguments
  if not arguments:
    acelib.printUsage("<log 1> <log 2> ...",
    "Converts trace logs into properly formatted JSON files")
  
  # Process each file from the command-line
  acelib.processFiles(arguments, translateToJson, "json")

def translateToJson(line):
  """Translate a single line from a trace-log into JSON"""
  # Parse the line
  table = acelib.parseTraceLogEntry(line)

  # Convert the line to JSON or return None if the line was un-parseable.
  if table:
    return json.dumps(table)
  else:
    return None

# Run main
if __name__ == "__main__": 
  acelib.run(main)
