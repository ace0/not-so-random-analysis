#!/usr/bin/python
# ACE - October 7, 2013

import acelib, json, numpy

description = \
"""
Reads JSON trace files, identifies dev-rand inputs and outputs, and computes the minimum
sequence length across all trace files.

@cutoff-ms:  0=no cutoff; otherwise: stop counting sequences after this many ms.
@arch:       [0|xen|vmware|ec2]  Used to select clock spped when cutoff is specified.
"""

# Architecture-specific details by platform.
cpuMhzTable = { 'xen'   : 2667, 
                'vmware': 3392, 
                'ec2'   : 1800
               }

def main(args):
  """
  Read all JSON traces and count sequence lengths.
  """
  # Check for at least one file on the command line and make sure the 
  # output file doesn't already exist.
  if len(args) < 3:
    acelib.printUsage('cutoff-ms arch <JSON trace files...>', description)

  # Grab arguments.
  cutoff_ms, arch, files = (int(args[0]), args[1], args[2:])

  # Convert us to ms and select the cpu MHz
  if cutoff_ms:
    cutoff_us = cutoff_ms * 1000
    cpuMhz = cpuMhzTable[arch]
  else:
    cutoff_us, cpuMhz = (None, None)

  # Build a matrix of the length of each sequence from each trace.
  # Trace 1: [ [ seq1-len, seq2-len, ... ], 
  # Trace 1:   [ seq1-len, seq2-len, ... ], 
  #           ...
  #          ]
  sequences = []

  # Process each trace file.
  for f in files:
    # Read all the input cycle counters and add them to the matrix.
    reader = SequenceCounter(cutoff_us=cutoff_us, cpuMhz=cpuMhz)
    sequences.append(reader.read(f))

  # Find the min-max value across all traces before we transpose the matrix.
  mm = minMax(sequences)

  # Transpose the matrix and print our results in CSV.
  sequences = acelib.transpose(sequences)
  print 'Sequence, Min Length, Max Length, Median Length'
  for i, s in enumerate(sequences):
    # Remove None values from the list (but don't remove 0s)
    s = [ item for item in s if item is not None ]
    
    # Print min, max, and median values.
    print '%d, %d, %d, %d' % (i, min(s), max(s), numpy.median(s))

  # Emit the min-max value as well.
  print 'min-max:', mm
  
 
def minMax(matrix):
  """
  Computes the min( [ max(row) ] ).  The minimum of all the maximums taken from each row.
  """
  maxes = [ max(row) for row in matrix ]
  return min(maxes)


class SequenceCounter(object):
  """
  Identifies dev-rand input and output events and counts the length of each input sequence.
  """
  # Function names of input and output events.
  inputEvents = ['add_interrupt_randomness', 'add_timer_randomness', 'add_device_randomness']
  outputEvents = ['get_random_bytes', 'urandom_read']


  def __init__(self, cutoff_us=None, cpuMhz=None):
    "Initialize a sequence reader with an optional @cutoff time in microseconds and @cpuMhz."
    # Set the max cycles if a cutoff was specified.
    if cutoff_us:
      self.max_cycles = cutoff_us * cpuMhz
    else:
      self.max_cycles = None


  def read(self, filename):
    """
    Reads a JSON file and returns a list of the length of each input sequence.
    """
    lengths = []
    count = 0
    start = True

    with open(filename, 'r') as f:
      # Read each line in the file.
      for line in f:
        # Convert the line from JSON into a dictionary.
        record = json.loads(line)

        # If we're using a cutoff, increment the max cycles value when we see 
        # the first record.
        if self.max_cycles and start:
          self.max_cycles += record['cycle_count']
          start = False

        # Discard records with an 'event' field to avoid double-counting events.
        if 'event' in record: continue
        
        # Check for output events - they start a new sequence.
        if record['function'] in self.outputEvents:
          lengths.append(count)
          count = 0

        # And count each input event.
        if record['function'] in self.inputEvents:
          count += 1

        # Quit if there's a valid cutoff and we've exceeded it.
        if self.max_cycles and record['cycle_count'] > self.max_cycles:
          break

    return lengths
## SequenceCounter

# Run our application
acelib.run(main)
