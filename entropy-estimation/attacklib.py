#!/usr/bin/python
# ACE - October 7, 2013

"""
Routines to support get_random_int attack preparation, analysis, and execution.
"""

import acelib, math, random

def relativeCyclesFromFiles(files):
  """
  Extracts cycle counters from JSON files, then extracts the relative cycle counts.
  """
  # Get the absolute cycle counts from the files.
  absoluteCycles = getCycleCounts(files)

  # Convert the cycle counters to relative values.
  return [ getRelativeCounters(item) for item in absoluteCycles ]
## relativeCyclesFromFiles

def getRelativeCounters(values, keepFirstAbsoluteCounter=True):
  """
  Convert a sequence of absolute cycle counters into a sequence of relative values where each
  cycle counter is replaced by the difference in cycles compared to the previous value.  If 
  @keepFirstAbsoulteCounter is set to true (default), then the first item in the list returned
  will be the first absolute cycle counter.  Otherwise, it will be set to 1.
  """
  # Keep the first absolute cycle counter the same.
  if keepFirstAbsoluteCounter:
    relatives = [values[0]]
  # Or set it to the constant value 1
  else:
    relatives = [1]

  # Remember the previous value.
  previous = None

  # Calculate relative values for each item in the list.
  for c in values:
    if previous:
      relatives.append(c-previous)
    previous = c

  # Return our resulting list of relative cycle counts.
  return relatives
## getRelativeCounters

def testRanges(ranges, matrix):
  """
  Test a given list of cycle ranges against a matrix of cycle counter values (each representing
  a call to get_random_int) returns a list of the fraction of attacks that would succeed at 
  each given attack depth.
  """
  # Keep track of the number of attack targets that survive at each depth.
  targets = []

  # Remember how many attack targets we started with.  Make this a float
  # so that the division we perform later will be floating point (not integer) division.
  total = float(len(matrix))

  # d = attack depth
  d = -1

  # Keep track of which files for which the attacks are still 'live'.
  live = matrix

  # Test each attack range against each trace at the correct depth.
  for s,e in ranges:
    d += 1
    live = testRange(s,e,d,live)
    targets.append(len(live))

  return [ i/total for i in targets ]
## testRanges

def testRange(start, end, depth, traces):
  """
  Test a given cycle range [@start-@end] against a @matrix of @traces at
  a given attack @depth.  Returns the subset of @traces for which the attack range succeeds.
  """
  # Test each attack range against each trace in the matrix at the specified depth.
  return [ t for t in traces if start <= t[depth] and t[depth] <= end ]
## testRange

def printLength(matrix): 
  """
  Prints the length of each row in a matrix for debugging purposes.
  """
  l = [ len(row) for row in matrix ]
  print l
## printLength

def prettyFloats(values):
  """
  Creates a list of 'pretty' floats that are 2-decimal point precision.
  """
  return ["%0.2f" % i for i in values]
## prettyFloats

def complexityFromRanges(cycleRanges):
  """
  Computes the average-case attack complexity assuming the cycle count range is
  uniformly distributed.
  """
  complexity = 1
  for s,e in cycleRanges:
    complexity *= max(1, int(round(0.5*(e-s))))
  return complexity
## complexityFromRanges

def train(matrix, coverage, depth, includeMedian=False):
  """
  'Train' an attack on a matrix of cycle counters associated with get_random_int
  calls.  The attack is configured for a given overall @coverage \in [0,1] and a targeted
  attack @depth.
  """
  # Determine the fraction of each cycle counter range (alpha) that we need to attack.
  alpha = getAlpha(coverage, depth)
  
  # For each call within the call depth, calculate the desired cycle counter range to
  # search.
  return [ calcRange(matrix[i], alpha, includeMedian) for i in range(depth) ]
## train

def calcRange(values, fraction, includeMedian=False):
  """
  Calculates the range, (start, end), from a list of integer @values that covers the
  given @fraction \in [0,1] of the values requested.  Uses a sliding window to 
  determine the range with minimum cycle counter range that is continous through 
  the list.
  """  
  # Sort the list.
  values.sort()
  
  # Determine the number of values inside a window.
  rangeLength = int(round(len(values) * fraction))

  # Start with the range being the whole list.
  # s = start index; e = end index
  s = 0
  e = rangeLength-1

  # Start with the current range as the best known range.
  minRange = (s, e, values[e] - values[s])

  while e < len(values):
    thisRange = values[e] - values[s]
    if thisRange < minRange[2]:
      minRange = (s, e, thisRange)
    s += 1
    e += 1
  ## while more windows

  # Grab the best known indices and report our range in cycle counters.
  s = minRange[0]
  e = minRange[1]
  
  # Grab (roughly) the middle index for the median, ignoring integer division or
  # averaging the two values for an even numbered list.
  m = len(values)/2
  
  if includeMedian:
    return (values[s], values[e], values[m])
  else:
    return (values[s], values[e])
## calcRange

def getAlpha(coverage, depth):
  """
  The desired range for a given call, alpha \in [0,1], is the fraction of 
  the cycle counter range that we're going to search.  It can be computed
  from the overall @coverage and attack @depth.
  """
  return math.pow(coverage, 1.0/depth)
## getAlpha

def getCycleCounts(files):
  """
  Reads the list of JSON files and returnsa (jagged) matrix of cycle counters 
  from each each call to get_random_int:
  file 1 ->  [ [c1, c2, c3, c4, ..., cn], 
  file 2 ->    [c1, c2, ...        , cn],
                ...
             ]
  """
  reader = CycleReader()
  reader.read(files)
  return reader.matrix
## getCycleCounts

def split(files):
  """
  Randomly (but repeatably) divides an even-numbered set of files into testing
  and training sets.  returns: ([test], [train])
  """
  if len(files) % 2 != 0: acelib.critical('There must be an even number of files')
  
  # Shuffle the list (randomly).
  random.shuffle(files)

  # Divide into two lists.
  x = len(files)/2
  return (files[:x], files[x:])
## split

class CycleReader:
  """
  Builds a martix of cycle counters from each call to get_random_int from a series of files.
  """
  def __init__(self):
    self.matrix = []
  
  def read(self, files):
    """ Reads a set JSON traces and extracts get_random_int cycle counters into a jagged matrix """
    acelib.processJsonFiles(files, self._screen, 
      onNewFile=self._onNewFile, printReadingFile=False)
  
  def _onNewFile(self, filename):
    """ Add a new empty list to the martix each time a new file is read """
    self.matrix.append([])
    
  def _screen(self, record):
    """ 
    Screen a record to see if it contains a get_random_int call.
    If so, grab it and insert it into the matrix.
    """
    if record['function'] == 'get_random_int' and 'cycles' in record:
      self.matrix[-1].append(record['cycles'])
## CycleReader

class GriReader:
  """
  Grabs all the get_random_int trace records from a given file.
  """
  def __init__(self, tracefile):
    """ 
    Read a JSON trace file and extract the get_random_int records.
    """
    self.records = []
    acelib.processJsonFiles([tracefile], self._screen, printReadingFile=False)
  
  def _screen(self, record):
    """ 
    Screen a record to see if it contains a get_random_int call.
    If so, keep it.
    """
    if record['function'] == 'get_random_int':
      self.records.append(record)
## GriReader


