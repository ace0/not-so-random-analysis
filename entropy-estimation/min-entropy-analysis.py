#!/usr/bin/python
# ACE - November 13, 2013

import acelib, ast
from amlib import *

description = \
"""
Computes sequence complexity (sigma_i) and max sequence complexity (lambda_i) for each
input sequence discarding correlated events and then
computing the min-entropy for each event (after masking away all but the lower bits 
alpha).
"""

# The maximum complexity for guessing the state of the dev-rand RNG is 1024 bits.
maxComplexity = 1024
outputKey = 'Output'

def main(args):
  """
  Read in the tables and compute complexity.
  """
  # Check arguments.
  if len(args) != 2: 
    acelib.printUsage('alpha-table event-table', description)

  # Unpickle the tables
  alphaTable, _ = acelib.unpickle(args[0])
  eventTable = acelib.unpickle(args[1])

  # Remove any entries with missing cycle counters
  alphaTable = dict((k,v) for k,v in alphaTable.iteritems() if '-1' not in k )
  
  # Get a list of input events.
  inputs = extractInputs(alphaTable)

  # Get a list of all the output events.
  outputs = eventTable[outputKey]

  # DEBUG: At some point we can remove this, but I have not yet seen an input
  #        that trips this test so I am suspicious of whether this test works.
  testOrdering(inputs)

  # Compute min-entropy for each inputs after masking away upper bits.
  # d for dropped input count
  d, hTable = computeH(inputs, alphaTable)

  # Compute complexity for each sequence
  results = computeComplexity(hTable, inputs, outputs)

  # Print the number of discarded inputs and the complexity results.
  print 'Total inputs=%d; Discarded inputs=%d' % (len(inputs), d)
  
  printResults(results, outputs)


def computeComplexity(hTable, inputs, outputs, debugStatus=False):
  """
  Computes the min and median sequence and max complexity values.
  """
  # The number of trials and the number of output sequences.
  trials = len(outputs[0])
  sequences = len(outputs)

  # We'll build 3 matrices of results, then extract
  # min/median values from each row of the matrix.
  # ell = length; s = sigma; l = lambda
  ell = emptyMatrix(trials, sequences)
  s   = emptyMatrix(trials, sequences)
  l   = emptyMatrix(trials, sequences)

  for t in range(trials):
    for i,elli,si,li in trialComplexity(hTable, inputs, outputs, t):
      if debugStatus:
        print 'Trial %s - Sequence %d: len: %d' % (t, i, li)
      ell[i][t] = elli
      s[i][t]   = si
      l[i][t]   = li

  # Aggregate our values and then zip them together.
  minEll, medEll = minMedian(ell)
  minS, medS = minMedian(s)
  minL, medL = minMedian(l)
  return zip(minEll, medEll, minS, medS, minL, medL)


def emptyMatrix(columns, rows, default=None):
  "Creates an empty matrix"
  return [ [default for _ in range(columns) ] for _ in range(rows) ]


def minMedian(matrix):
  """
  Computes two lists: (mins, medians) which represent the min/median value
  from each row in a @matrix.
  """
  mins = [ min(row) for row in matrix ]
  medians = [ numpy.median(row) for row in matrix ]
  return mins, medians


def trialComplexity(hTable, inputs, outputs, trial):
  """
  This generator yields (i,ell,sigma,lambda) for each sequence @i in a 
  @trial (column in the event table).  @ell is the length of sequence @i;
  @sigma is the sum of the logs of the min-entropy for each input in the 
  sequence; and @lambda is the maximum @sigma seen so far for this trial.
  Both @sigma and @lambda are capped at maxComplexity.  
  """
  # l for lambda
  l = 0

  # Copy the inputs list because we're going to remove items from it.
  inputs = list(inputs)

  # Compute details for the sequence preceding each output
  for i, output in enumerate(outputs):

    # Compute sequence complexity
    s, ell = sequenceComplexity(trial, hTable, inputs, output[trial])

    # Compute lambda as the max seen so far (but capped at maxComplexity)
    l = min(maxComplexity, max(l, s))

    # Report values for each sequence
    yield i,ell,s,l


def sequenceComplexity(trial, hTable, inputs, time):
  """
  Computes the sequence complexity (sigma) and the sequence length (ell) 
  as the sum of the min-entropy values for inputs that occur before the specified
  output @time for a given @trial.  Min entropy values are taken from @hTable and
  all preceding inputs are removed from the list of @inputs.
  """
  # s for sigma, ell for length
  s = 0.0
  ell = 0

  # Make a copy of inputs to iterate over.
  # Then check for preceding inputs.  c for candidate.
  for c in list(inputs):
    # Check for inputs that precede this output time.
    if c[trial] < time:
      s += lg( hTable[str(c)] )
      ell += 1
      inputs.remove(c)

  return s, ell


def computeH(inputs, alphaTable, threshold=0.4, debug=False):
  """
  Computes the min entropy (H_\inf) of each input after masking the lower bits
  according to the @alphaTable.  If any input shows a correlation coeff above
  the @threshold, then the min entropy of that input is set to 0.

  Returns (d, hTable)
  @d: The number of inputs whose maxCC exceeded the @threshold
  @hTable: a table that maps inputs to min entropy values.
  """  
  # A table in min entropy values
  hTable = {}

  # Keep track of the number of inputs that are dropped because of tight
  # correlations.
  dropCount = 0
  
  # Compute H_\inf for each input i
  for i in inputs:
    # The key to the alpha table and the hTable is the string repr
    # of each input. k for key.
    k = str(i)

    # a for alpha
    a = alphaTable[k]

    if debug:
      print '============================'
      print
      print 'alpha:', a

    # Check for max CC above the threshold
    if maxCC(i, a, inputs, threshold) > threshold:
      hTable[k] = 0.0
      dropCount += 1
      print 'Maxxed out'

    # Otherwise, min-entropy is the number of unique elements after
    # applying the mask
    else:
      hTable[k] = len(set(maskLowerBits(i, a)))

    if debug:
      print 'H_oo:', hTable[k] 

  return dropCount, hTable


def maxCC(input, alpha, allInputs, threshold=1.0):
  """
  Computes the maximum correlation coefficient between the lower @alpha bits
  of each value in @inputs and all input events in @allInputs that strictly proceed
  @input.
  """
  # Start small
  maxRho = 0.0

  # Check correlation coefficients for all preceding events in allInputs
  # c for candidate
  for c in allInputs:
    # Check for early termination -- if all items in are strictly larger than
    # all items in input, we assume there are no more preceding events
    if gt(c, input):
      break

    # If this is preceding event, comput the corr coeff.
    if lt(c, input):
      rho = maskCC(input, c, alpha)
      maxRho = max(rho, maxRho)

    # Once we reach the threshold, we don't require any more evidence.
    if maxRho >= threshold:
      break

  # Report the maximum CC
  return maxRho


def maskCC(a,b, alpha, maskBoth=True, debug=False):
  """
  Masks for the lower @alpha bits of @a and computes a pearson CC against b.
  if @maskBoth is True, then pearson CC is computed for both pairs:
  (masked a, b) and (masked a, masked b) and the maximum value is returned.
  """
  # Mask list a and compute CC against list b
  maskedA = maskLowerBits(a, alpha)
  rho1 = abs(scipyCC(maskedA, b))
  rho2 = 0.0

  # If requested, compute CC against masked values of b
  if maskBoth:
    maskedB = maskLowerBits(b, alpha)
    rho2 = abs(scipyCC(maskedA, maskedB))

  # Print CC results using various applications of masks
  if (debug):
    print 'alpha:   ', alpha 
    print 'No mask: ', scipyCC(a,b)
    print 'Mask a:  ', scipyCC(maskLowerBits(a, alpha), b)
    print 'Mask a+b:', scipyCC(maskLowerBits(a, alpha), maskLowerBits(b, alpha))
    print 'Max:    :', max(rho1, rho2)
    print

  # Report the maximum of the two CC values
  return max(rho1, rho2)


def extractInputs(table):
  """
  Extracts a list of event cycle counter lists from an alpha table and orders these
  inputs in (roughly) using a totoal ordering of all items in each input.
  """
  # Extract the keys and convert them from strings to proper lists.
  inputs = [  ast.literal_eval(k) for k in table.keys() ]

  # Shave the matrix to ensure all rows are equi-length.
  inputs = shave(inputs)

  # Sort these lists by full ordering of each item in the list.
  return sorted(inputs, cmp=lt)


def testOrdering(matrix):
  """
  Tests the ordering of row in @matrix to determine if the first appearance
  of r_j > r_i (for j > i) is sufficient to conclude that all following rows
  r_k have the propery that: !(r_k < r_i).
  Sounds complicated, but this is just checking that we can stop looking for
  _preceding_ events once we see a row r_j > r_i.
  """
  # Run this test for all rows in the matrix
  for a in matrix:
    # Signifies when we think we've found the last event that needs
    # to be examined
    foundLast = False

    # Test against all row in the matrix.
    for b in matrix:
      # Check for the bad case -- a preceding event after we've found
      # what we thought was the early termination condition
      if foundLast and lt(b,a):
        print 'ERROR: Found preceding row when no more preceding rows were expected'
        print 'a:', a
        print 'b:', b
        print

    # Check for the first early-termination condition.
    if not foundLast and gt(b,a):
      foundLast = True


def lt(a,b):
  """
  Compares two lists for strict ordering of all elements: a_i < b_i (\forall i \in [len(a,b)])
  """
  for i,j in zip(a,b):
    # Check for counter examples
    if not i < j: 
      return False

  # Otherwise, these lists pass
  return True


def gt(a,b):
  """
  Compares two lists for strict ordering of all elements: a_i > b_i (\forall i \in [len(a,b)])
  """
  for i,j in zip(a,b):
    # Check for counter examples
    if not i > j: 
      return False

  # Otherwise, these lists check out
  return True


def printResults(results, outputs):
  """
  Print out results as a CSV
  """
  print 'i, max output cc, min ell_i, med ell_i, min sigmna_i, med sigma_i, min lambda_i, med lambda_i'
  for i, r in enumerate(results):
    # Grab the max cycle counter for this output value
    cc = max(outputs[i])
    print acelib.commas([i,cc] + list(r))


# Run!
acelib.run(main)
