#!/usr/bin/python
# ACE - December 20, 2013

import math, numpy, random, scipy.stats

"""
Contains routines for computing statistical tests.
"""

# From: http://eee.guc.edu.eg/Courses/Networks/NETW707%20Modelling%20&%20Simulation/Lectures/Lecture%209%20-%20Tests%20for%20Random%20Numbers.pdf
# Numerator constants for computing critical D-values for the Kolmogorov-Smirnov Test
# for a selection of signifcance values (alpha-values in KS parlance).
# { alpha: c(alpha) }
ksCriticalConstantTable = \
{ 0.100: 1.22, 
  0.050: 1.36,
  0.010: 1.63, 
  0.005: 1.73, 
  0.001: 1.95
}

def ksCriticalValue(signifigance, samples):
  """
  Computes the critical D-value for the KS test for a given @signifigance level and a given number
  of empricial @samples.
  """
  # n for numerator.
  n = ksCriticalConstantTable[signifigance]
  return n/math.sqrt(samples)


def ksFromMatrix(matrix, size, unitTest=False):
  """
  Computes the KS test for uniformity over each row in a @matrix for
  a given expected @size (max value).
  """
  # Run unit test if requested.
  if unitTest:
    print "========================================================"
    print
    print " Results are from random generated unit test values"
    print
    print "========================================================"
    return [ ksUnitTest(size, len(matrix[0])) for row in matrix ]

  # Otherwise, run the ks test on each row of the matrix.
  else:
    return [ ksTest(row, size) for row in matrix ]


def ksUniform(values, size, signifigance=0.1):
  """
  Returns True or False whether or not the specified list of 
  @numeric values passes the KS test for uniformity at a given
  @signifigance level.  
  @size is the maximum number of unique values expected in the uniform 
  distribution.
  """
  # Compute the critical D-value, compute the KS test, and comnpare
  # the test metric (D) to the critical D-value.
  dCritical = ksCriticalValue(signifigance, len(values))
  d, p = ksTest(values, size)

  # Compare the critical value and p value.
  #return (d <= dCritical and p > signifigance)
  return (d <= dCritical)


def ksTest(values, size):
  """
  Computes the KS test for uniformity over a list of numeric @values 
  that are exected to be in the range 0 to @size.  Returns (D, p)
  D is the KS test metric, and p is the statistical signifigance leve (p-value).
  """
  # Generate the uniform distribution over the given range.
  cdf(values)
  u = scipy.stats.uniform(loc=0, scale=size-1)
  return scipy.stats.kstest(values, u.cdf)


def ksUnitTest(size, samples):
  "Runs ksTest against a number of samples taken from the python random()"
  # Generate the requested number of samples and run them through KS test.
  values = [ random.randint(0, size-1) for _ in range(samples) ]
  return ksTest(values, size)


def cdf(values, debug=False):
  "Computes the cumulative distribution function (CDF) over a range of values"
  # Start with the histogram, then compute the CDF.
  h, _ = numpy.histogram(values)

  # Print values for degugging:
  if debug:
    print 'histogram:',  h
    print 'cdf:      ', numpy.cumsum(h)

  # Compute the CDF of this histogram.
  return numpy.cumsum(h)

