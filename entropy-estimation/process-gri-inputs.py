#!/usr/bin/python
# extract-sequence-cycles.py
#
# ACE - October 7, 2013

import acelib, collections, json, os

description = \
"""
Reads a JSON trace file, extracts cycle counters for get_random_int events, and 
pickles the resulting table that maps events to a matrix of observed relative cycle counters.
"""

def main(args):
  """
  Read all JSON traces and extract cycle counters for \devurand inputs and outputs.
  """
  # Check for at least two file arguments an ensure the output file doesn't already exist.
  if len(args) < 3 or os.path.isfile(args[0]):
    acelib.printUsage('<output file> <max outputs> <JSON trace args>...',   
            description)

  # Grab arguments.
  output, maxOutputs, traces = args[0], int(args[1]), args[2:]
    
  # Keep a matrix of GRI input/output cycle counters from each trace.
  results = { 'Output':[] }

  # Process each trace file.
  for f in traces:
    # Read all GRI events from this file and add them to the dictionary.
    reader = CycleReader(maxOutputs)
    reader.read(f)
    results['Output'].append(reader.cycles)

  # Transpose the results and write them to a file.
  acelib.pickle(output, transposeTable(results))


def transposeTable(d):
  """
  Transposes each matrix in an event table of matrices and returns a new table.
  """
  table = {}
  for key, matrix in d.items():
    table[key] = acelib.transpose(matrix)
  return table
  

class CycleReader(object):
  """
  Captures relative cycle counters for get_random_int events./
  """
  def __init__(self, maxOutputs):
    # A list of relative cycles for each input event.
    self.cycles = []
    self._start = None
    self.maxOutputs = maxOutputs


  def read(self, filename):
    """
    Reads a JSON file and extracts cycle counters from input events.
    """
    with open(filename, 'r') as f:
      # Read each line in the file.
      for line in f:
        # Quit reading once we hit our maximum number of output events.
        if len(self.cycles) >= self.maxOutputs:
          return

        # Convert from JSON to a record (dictionary) and process this record.
        self._process(json.loads(line))
      ## for each line
    ## with file


  def _process(self, record):
    """
    Grabs and stores all relative cycle counters for GRI events.
    """
    # Note the first cycle counter in the file.
    if not self._start: 
      self._start = getCycles(record)

    # Ignore everything except GRI events.
    if record['function'] != 'get_random_int' or 'event' in record:
      return

    # Store each cycle counter as a relative value.
    self.cycles.append(getCycles(record) - self._start)
## CycleReader

def getCycles(record):
  """
  Grabs the absolute cycle counter from a trace giving preference to 'cycles' over 'cycle_count'
  when it's available.
  """
  if 'cycles' in record:      return record['cycles']
  if 'cycle_count' in record: return record['cycle_count']
  else:                       return None


# Run our application
acelib.run(main)
