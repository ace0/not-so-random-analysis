#!/usr/bin/python
# ACE - November 13, 2013

import acelib, copy, math, numpy, random, scipy.stats, statslib, sys
from amlib import *

description = \
"""
Computes sequence complexity (s_i) and max sequence complexity (kappa_i) for each
input sequence from a pickled event table and reports the min and median values for
each sequence.  Sequence complexity is based on the min-uniform alpha for each event sequence.
"""

# The maximum complexity for guessing the state of the dev-rand RNG is 1024 bits.
maxComplexity = 1024
outputKey = 'Output'

# Slight change to computation for processing GRI events.
gri = False


def main(unused):
  """
  Find the minimum number of uniformly distributed lower bits for each input.
  """
 # Check arguments.
  options, args = acelib.parseCommandLine()

  if len(args) != 1: 
    acelib.printUsage('[--gri] [--alphas=pickled-alpha-file] pickled-event-table', 
                      description)

  # Set  the GRI flag when requested.
  if options['gri']:
    global gri, defaultRange
    gri = True

  # Process our arguments.
  table = acelib.unpickle(args[0])

  # Remove empty values from the table.
  for key, matrix in table.items():
    table[key] = markInvalid(matrix)
  
  # Compute complexity results across all the trials.  
  results = computeComplexity(table)

  # Print the alpha table or the complexity results.
  if options['alphas']:
    acelib.pickle(options['alphas'], (minAlphaTable, ccDeltaTable))

  else:
    printResults(results, table)


def printResults(results, table):
  """
  Print out results as a CSV
  """
  print 'i, max output cc, min l_i, med l_i, min s_i, med s_i, min kappa_i, med kappa_i'
  for i, r in enumerate(results):
    # Grab the max cycle counter for this output value
    cc = max(table[outputKey][i])
    print acelib.commas([i,cc] + list(r))


def markInvalid(matrix, d=-1):
  """
  Replace any None values in @matrix with the default value @d.
  """
  def mark(values, d):  return [ int(x or d) for x in values ]
  return [ mark(row, d) for row in matrix ]


def computeComplexity(table, debugStatus=False):
  """
  Computes the min and median sequence and max complexity values.
  """
  # The number of trials and the number of output sequences.
  trials = len(table[outputKey][0])
  sequences = len(table[outputKey])

  # We'll build 3 matrices of results, then extract
  # min/median values from each row of the matrix.
  l = emptyMatrix(trials, sequences)
  s = emptyMatrix(trials, sequences)
  k = emptyMatrix(trials, sequences)

  for t in range(trials):
    for i,li,si,ki in trialComplexity(table, t):
      if debugStatus: 
        print 'Trial %s - Sequence %d: len: %d' % (t, i, li)
      l[i][t] = li
      s[i][t] = si
      k[i][t] = ki

  # Aggregate our values and then zip them together.
  minL, medL = minMedian(l)
  minS, medS = minMedian(s)
  minK, medK = minMedian(k)
  return zip(minL, medL, minS, medS, minK, medK)


def emptyMatrix(columns, rows, default=None):
  "Creates an empty matrix"
  return [ [default for _ in range(columns) ] for _ in range(rows) ]


def minMedian(matrix):
  """
  Computes two lists: (mins, medians) which represent the min/median value
  from each row a matrix.
  """
  mins = [ min(row) for row in matrix ]
  medians = [ numpy.median(row) for row in matrix ]
  return mins, medians


def trialComplexity(table, trial):
  """
  This generator yields (i,l,s,kappa) for each sequence i in a given @trial.
  l is the length of the sequence, s is the sequence complexity, and k is the
  sum of the previous s_i's.  Both s and kappa are capped at maxComplexity.
  """
  if gri:
    inputTable = {}
  else:
    # Clone the table and remove output events.
    inputTable = copy.deepcopy(table)
    inputTable.pop(outputKey)

  # Compute the sequence complexity for each output.
  k = 0
  for i, output in enumerate(table[outputKey]):
    # Compute s = seq complexity and max value seen so far (k=kappa).
    l, s = sequenceComplexity(output, table, inputTable, trial)
    
    # GRI computes kappa as the sum of previous s values
    if gri:
      k = min(maxComplexity, k+s)
    # dev-rand uses the max value
    else:
      k = min(maxComplexity, max(k, s))
    yield i,l,s,k
  

def sequenceComplexity(output, eventTable, inputTable, trial):
  """
  Computes the sequence complexity (up to the maxComplexity) for a given
  @output and @trial.  @eventTable is the original event table for finding
  minUniformAlpha.  @inputTable contains all inputs that have not been allocated
  to another sequence.  Inputs used in this sequence will be removed from 
  @inputTable.
  Returns: len(sequence), complexity

  If gri=True: we are performing calculations on GRI sequences, so the input
  sequence is a single row that is also the output row.
  """
  if gri:
    sequence = [output]
  else:
    # Find and remove inputs that are the sequence for this output and trial.
    sequence = inputSequence(output, inputTable, trial)

  # Ensure we have a regular matrix.
  sequence = shave(sequence)
  
  # c for complexity (log2).  Compute: c = SUM minUniformAlpha(input_i)
  c = 0
  for row in sequence:
    c += lookupMinAlpha(row, eventTable) 

    # Quit once we hit the max.
    if c >= maxComplexity: 
      c = maxComplexity
      break    

  return len(sequence), c


def inputSequence(output, inputTable, trial):
  """
  Computes a matrix of all input events from the @inputTable that
  occur before a given output event and trial.  These events are removed
  from the input table.
  """
  sequence = []
  # Check each row of each event matrix in the table.
  for event, matrix in inputTable.iteritems():
    # Find the chopping point for this trial to split the matrix of event times.
    # s for split-point
    s = 0
    for i, row in enumerate(matrix):

      # Skip any inputs that were not recorded for this trial.
      # Or any negative cycle counter values.
      if len(row) <= trial or row[trial] < 0:
        # acelib.warn('Short row' + str(len(row)))
        break

      # If this input event occurs before our output event, grab the row
      # from the table.
      if row[trial] < output[trial]:
        s += 1
        
      # Otherwise, there are no more events in this matrix that occur after
      # before the output time for this trial.
      else:
        break

    # Split the list into events that occur before the output and those that 
    # occur after. Put the after events back into the table and add the before
    # events to our input sequence.
    before = matrix[:s]
    after = matrix[s:]
    
    if before:
      sequence.extend(before)
    inputTable[event] = after

  # Report this matrix of input events.
  return sequence


# Caches min-uniform-alpha values by event to enable fast processing.
minAlphaTable = {}

# Keeps the cycle counter deltas and their min-alpha values indexed
# by the original ccycle counter list.
# { [cc, cc, ...] : { (alpha, [cc_delta, cc_delta, ....]) }
ccDeltaTable = {}

def lookupMinAlpha(values, table, cheat=False):
  """
  Finds the min-alpha of the list of @values using all offsets in an event @table.
  """
  ## CHEAT! To test long-running scripts.
  if cheat: 
    return 0
  
  # Check the table, if it's not there, compute it and cache it.
  key = str(values)
  if key not in minAlphaTable:
    (minAlphaTable[key], ccDeltaTable[key]) = minAlphaFromTable(values, table)
  
  # Report the cached value.
  return minAlphaTable[key]


def minAlphaFromTable(values, table):
  """
  Find the min-alpha of the list of @values using all offsets in an event @table.
  """
  minAlpha = None

  # Grab the matrix for each event type.
  for matrix in table.values():

    # minUniformAlpha 
    alpha = minUniformAlpha(values, matrix) 

    # If we ever hit 0, we don't need to continue.    
    if alpha == 0:
      return 0

    # Keep the first result as the starting min value
    if not minAlpha: minAlpha = alpha
    minAlpha = min(minAlpha, alpha)

  return minAlpha


# TODO:
# Merge this with ks-complexity and take a --test=[ks|chi2]
# Combine maxAlpha with uniformAlpha and just swap out different
# tests since the interface to both tests is very similar.

def minUniformAlpha(values, matrix):
  """
  Tests all possible (positive, non-zero) offsets for the cycle counter @values
  using each row in @matrix and determines the maximum number of lower bits (alpha)
  that passes a KS test for uniformity.  Each offset is computed and the minimum
  alpha value is returned.
  """
  # Start with no offset as the baseline value.
  minAlpha = csMaxAlpha(values)

  # Cycle counter deltas at the minimum alpha
  minDeltas = values

  for row in matrix:
    # d for deltas: offsets from this row
    d = positiveDeltas(row, values)

    # Compute uniform alpha
    if d:
      a = csMaxAlpha(d)

      # Quit if we ever see 0.
      if a == 0:
        return (0, [])

      # Check for a new minimum value.
      if a < minAlpha:
        minAlpha = min(minAlpha, a)
        minDeltas = d

    # Our matrix comes in order, quit the first time we see an 
    # unusable time discrepancy.
    else:
      break

  # Report the minimum alpha and associated deltas
  return minAlpha, minDeltas
     

def positiveDeltas(a, b):
  """
  Computes @b-@a and returns either: 
  -> b-a   if all bi > ai
  -> None  otherwise
  """
  # d for deltas
  d = [0]*len(a)

  # Start simple - we need to vectors of equal dimension.
  if len(a) != len(b): return None
  
  # Compute deltas individually and quit if we see any non-positive values.
  for i in range(len(a)):
    d[i] = b[i] - a[i]
    if d[i] <= 0: 
      return None

  # Success!  All positive deltas!
  return d

# TODO: this test uses p value, but KS-test uses a critical value.
# Lookup the critical values and try using those in place of p-values.
def csMaxAlpha(values, limit=24, signifigance=0.1):
  """
  Finds the maximum number of lower bits (alpha) that pass the chi-squared
  test against a uniform distribution for a group of samples.
  """
  for alpha in range(limit, 0, -1):
    # Mask all but lower bits and run the test
    bins = min(10, max(2, alpha))
    _, p = chiSquare(maskLowerBits(values, alpha), 0, 2**alpha-1, bins)
        
    # If the p-value exceeds the signifigance level, we call it a pass.
    if p > signifigance:
      return alpha

  return 0


def chiSquare(values, start, end, bins=4, minFrequency=5, debug=False):
  """
  Computes the chi-squared test over a list of numeric @values using the specified
  number of @bins.  Once frequencies have been computed over the list of values,
  an error is printed if any of the frequencies is less than the @minFrequency.
  Returns: (chiSquare, p)
  chiSquare: test value
  p: statistical signifigance of this test
  """
  # Bin the values.  h for histogram.
  h, _ = numpy.histogram(values, bins, range=(start, end))

  # Verify that all frequencies are above the minimum.
  if minFrequency and min(h) < minFrequency:
    if debug:
      acelib.warn('One or more frequencies below the desired minimum value ({0}). ' \
                    'Frequencies:{1}'.format(minFrequency, h))
    return 0,0
  
  return scipy.stats.chisquare(h)


# Run our application
acelib.run(main)
