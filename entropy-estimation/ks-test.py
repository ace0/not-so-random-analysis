#!/usr/bin/python
# ACE - March 31, 2014

import math, numpy, random, scipy.stats, sys

description = \
"""
Determines if the provided hexadecimal values (individually) pass the Kolmogorv-Smirnov 
test for uniformity when each byte value is interpreted as an unsigned integer selected 
from the range [0,255].
"""

def main(unitTest=False):
  """
  Reads hexadecimal values from STDIN, interprets them as 1-byte values, and applies a KS-test to
  each sequence of bytes.  If @unitTest is true, skips all inputs and just runs a unit test.
  """
  # Check for requested unit test.
  if unitTest:
    ksUnitTest()
    return

  # Process each line on input.
  for line in sys.stdin:
    processHex(line.strip())


def processHex(hex, debug=False):
  """
  Convert a @hex string into 1-byte integers and apply a KS-test over the collection
  of bytes.  Prints an error message if the string contains non-hex characters.
  """
  # Chop the string into 2 character substrings.
  twosies = [hex[i:i + 2] for i in range(0, len(hex), 2)]

  if debug:
    print twosies

  # Interpret each byte as an int; v for values
  try:
    v = [ int(x, 16) for x in twosies ]
  except:
    print 'ERROR: %s contains non-hexadecimal characters' % hex
    return

  # size: the maximum number of values we expect to see (in a uniform distribution 
  # over 1-byte range) is 2^8 = 256
  if ksUniform(v, 256):
    print "%s: PASS (appears random)" % hex
  else:
    print "%s: FAIL (appears non-random)" % hex


def ksUnitTest(sampleCount=8, sampleRange=256, trials=1000):
  """
  Test our KS test on @sampleCount number of samples taken from the random number generator.
  (Nearly) all outputs should report "PASS" if the KS test is working and valid for this 
  number of samples and this range.
  """
  # Run a number of tests and report them percentage of trials that passed.
  passes = 0
  for i in range(trials):
    # v for values
    v = [ random.randint(0, sampleRange) for _ in range(sampleCount) ]
    if ksUniform(v, sampleRange):
      passes += 1

  print "PASS %0.3f%%" % (float(passes)/trials)


def ksUniform(values, size, signifigance=0.01):
  """
  Returns True or False whether or not the specified list of 
  @numeric values passes the KS test for uniformity at a given
  @signifigance level.  
  @size is the maximum number of unique values expected in the uniform 
  distribution.
  """
  # Compute the critical D-value, compute the KS test, and compare
  # the test metric (D) to the critical D-value.
  dCritical = ksCriticalValue(signifigance, len(values))
  d, p = ksTest(values, size)

  # Compare the critical value.
  return (d <= dCritical)


def ksTest(values, size):
  """
  Computes the KS test for uniformity over a list of numeric @values 
  that are exected to be in the range 0 to @size.  Returns (D, p)
  D is the KS test metric, and p is the statistical signifigance leve (p-value).
  """
  # Generate the uniform distribution over the given range.
  cdf(values)
  u = scipy.stats.uniform(loc=0, scale=size-1)
  return scipy.stats.kstest(values, u.cdf)


# From: http://eee.guc.edu.eg/Courses/Networks/NETW707%20Modelling%20&%20Simulation/Lectures/Lecture%209%20-%20Tests%20for%20Random%20Numbers.pdf
# Numerator constants for computing critical D-values for the Kolmogorov-Smirnov Test
# for a selection of signifigance values (alpha-values in KS parlance).
# { alpha: c(alpha) }
ksCriticalConstantTable = \
{ 0.100: 1.22, 
  0.050: 1.36,
  0.010: 1.63, 
  0.005: 1.73, 
  0.001: 1.95
}


def ksCriticalValue(signifigance, samples):
  """
  Computes the critical D-value for the KS test for a given @signifigance level and a given number
  of empricial @samples.
  """
  # n for numerator.
  n = ksCriticalConstantTable[signifigance]
  return n/math.sqrt(samples)


def cdf(values, debug=False):
  "Computes the cumulative distribution function (CDF) over a range of values"
  # Start with the histogram, then compute the CDF.
  h, _ = numpy.histogram(values)

  # Print values for degugging:
  if debug:
    print 'histogram:',  h
    print 'cdf:      ', numpy.cumsum(h)

  # Compute the CDF of this histogram.
  return numpy.cumsum(h)


# Run our application
if __name__ == "__main__":
    main()
