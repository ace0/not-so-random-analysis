/**
 * rnd-ioctl.h
 *
 * Copyright (C) 2013 - Adam C Everspaugh
 * All rights reserved.
 */
#ifdef __LINUX__
# include <linux/types.h>
# include <linux/ioctl.h>
# include <linux/irqnr.h>
# include "random.h"
#endif

#include <sys/ioctl.h>
#include <sys/types.h>
#include <sys/stat.h>
#include "rnd-tools.h"

#ifdef __LINUX__
	// Retrieve the entropy count for the specified file descriptor.
	static inline
	int get_entropy_count(int fd, int cmd)
	{
		// Call ioctl, verify our result, and return the count.
		ulong count = -1;
		int result = ioctl(fd, cmd, &count);
		return (result == OK) ? count : result;
	}
#else
	// Returns -1 everytime for non-Linux platforms.
	static inline
	int	get_entropy_count(int fd, int cmd)
	{
		return -1;
	}
#endif
