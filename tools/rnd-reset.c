/**
 * dev-rnd-reset.c
 * Performs /dev/random RNG logging reset on an instrumented RNG using IOCTL.
 *
 * Copyright (C) 2013 - Adam C Everspaugh
 * All rights reserved.
 */
#include <unistd.h>
#include "rnd-ioctl.h"

int main()
{
  // Open the device, perform the reset, and confirm the result.
  int fd = open_device(DEV_RANDOM);
  int result = ioctl(fd, RNDTRACERESET);
  confirm(result == OK, "ioctl error");

  // Close the file and report success.
  close(fd);
  return OK;
}
