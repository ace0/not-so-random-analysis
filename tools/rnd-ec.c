/**
 * rnd-ec.c
 * Queries and prints the entropy count for /dev/random with an IOCTL.
 *
 * Copyright (C) 2013 - Adam C Everspaugh
 * All rights reserved.
 */
#include <unistd.h>
#include "rnd-ioctl.h"

// Queries the entropy counts using the given file descriptor and prints them to STDOUT.
void print_entropy_count(int fd)
{
  // Query and print the entropy count for each pool.
  const char const* message = "%s entropy count: %d\n";
  ulong count = -1;

  // Input pool.
  count = get_entropy_count(fd, RNDGETENTCNT);
  printf(message, "input pool", count);

  // Blocking pool.
  count = get_entropy_count(fd, RNDGETENTCNT_BL);
  printf(message, "blocking pool", count);

  // Nonblocking pool.
  count = get_entropy_count(fd, RNDGETENTCNT_NB);
  printf(message, "nonblocking pool", count);
  printf("\n");
}

// Endlessly probes and prints the entropy count until terminated.
int main(void)
{
  // Open the device.
  int fd = open_device(DEV_RANDOM);

  // Print the entropy count until an error or forever.
  while (1)
  {
  	print_entropy_count(fd);
  	sleep(1);
  }
  return ERROR;
}
