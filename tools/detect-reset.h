/**
 * reset-detector.h
 *
 * Copyright (C) 2013 - Adam C Everspaugh
 * All rights reserved.
 * 
 * Use the cycle counter to detect when a VM has been reset from a snapshot
 * by detecting the large discrepancy in reported cycle counter.
 */
#ifndef DETECT_RESET_H_
#define DETECT_RESET_H_

#include "rnd-tools.h"

// Threshhold for detecting VM reset
// Currently: 6600000000 ticks = 2s on a 3.3 GHz CPU when using RDTSC
#define MAX 6600000000ull


// Waits in a loop until a reset occurs.
// @wait_us  The number of microseconds to sleep between checking the 
//           cycle counter.
static inline void wait_for_reset(unsigned long wait_us)
{
  u64 previous = rdtsc();
  while (1)
  {
    // Sleep and measure the elaspsed number of cycles.
    usleep(wait_us);
    u64 current = rdtsc();
    
    // If we see a huge jump in cycle counter, then we've detected a VM-reset.
    if ((current-previous) > MAX) 
      return;

    // Otherwise, keep checking.
    previous = current;
  }
}

#endif // DETECT_RESET_H_
