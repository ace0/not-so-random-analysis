/**
 * rnd-time.c
 * Times the reading of data from /dev/(u)random and outputs results in CSV format.
 *
 * Adam C Everspaugh
 * Copyright (C) 2013 - All rights reserved.
 */
#include <stdio.h>
#include "rnd-tools.h"

#define BUFFER_SIZE (5*MB)
static u8 buffer[BUFFER_SIZE];

/**
 * Reads samples from a /dev/(u)random device and measures the read timing in
 * cycles.
 */
int main(int argc, char** argv)
{
  // Check for arguments.
  if (argc != 4)
  {
    printf(
   "Usage: rnd-time <samples> <sample size> <RNG>\n\n"
   "samples:          The number of samples to take from the RNG\n"
   "sample size:      Size of sample (bytes) to take from the RNG\n"
   "RNG:              Selects the RNG to sample: 1=/dev/urandom, 2=/dev/random\n"
    );
    return ERROR;
  }

  // Parse our arguments.
  uint samples     = atoi(argv[1]);
  uint sample_size = atoi(argv[2]);
  int rng          = atoi(argv[3]);
  char* RNG = (rng == 1) ? DEV_URANDOM : DEV_RANDOM;

  // Verify sample size is within range.
  confirm(sample_size <= BUFFER_SIZE,
  		"Sample size cannot exceed %d bytes.", BUFFER_SIZE);

  // Open the device.
  int fd = open_device(RNG);

  // Print a CSV header.
  printf("Sample, elapsed cycles\n");

  // Print the entropy count until an error or forever.
  for (int i=1; i <= samples; ++i)
  {
  	// Perform a read and time it.
  	u64 start = rdtsc();
  	read_device(fd, buffer, sample_size);
  	u64 end = rdtsc();

  	// Print the elapsed number of cycles.
  	printf("%d, %llu\n", i, (end-start));
  }
  return OK;
}
